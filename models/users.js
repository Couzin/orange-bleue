const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
    firstname: { type: String, required:true },
    lastname: { type: String, required:true },
    email: { type: String, required:true },
    birthdate: { type: Date, required:true },
    adress: { type: String, required:true },
    postCode: { type: String, required:true },
    city: { type: String, required:true },
    password: { type: String, required:true },
    access: { type: String, default:'member' },
}) 

let users = mongoose.model("users", mySchema);

module.exports = users;