const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
    user: { type: String, required:true },
    userName: { type: String, required:true },
    nbcard: { type: BigInt, required:true },
    validDate: { type: String, required:true },
    cvc: { type: Number, required:true },

    
}) 

let paymentMethods = mongoose.model("paymentMethods", mySchema);

module.exports = paymentMethods;