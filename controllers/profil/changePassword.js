const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.put('/change-password', (req, res) => {
  
  const userId = req.body.userId; 
  const newPassword = req.body.newPassword;

  User.findByIdAndUpdate(userId, { password: newPassword }, (err, user) => {
    if (err) {
      return res.status(500).json({ error: 'Erreur lors de la mise à jour du mot de passe' });
    }

    if (!user) {
      return res.status(404).json({ error: 'Utilisateur non trouvé' });
    }

    return res.json({ success: 'Mot de passe mis à jour avec succès' });
  });
});

app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});
